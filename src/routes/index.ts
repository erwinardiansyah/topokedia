import Home from '../containers/Home'
import PokemonDetail from '../containers/PokemonDetail'
import PokemonList from '../containers/PokemonList'
import MyPokemonList from '../containers/MyPokemonList'
import HelloGoodbye from '@components/Hello/Goodbye'

export const routes = [{
  component: Home,
  exact: true,
  path: '/',
}, {
  component: HelloGoodbye,
  exact: true,
  path: '/logout',
}, {
  component: PokemonDetail,
  exact: true,
  path: '/pokemon-detail/:name',
}, {
  component: PokemonList,
  exact: false,
  path: '/pokemon-list/',
}, {
  component: MyPokemonList,
  exact: false,
  path: '/my-pokemon-list/',
}]
