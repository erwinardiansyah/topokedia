import React from 'react'

import {
  Card,
  CardMedia,
  CardContent,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography,
} from '@material-ui/core'

import { ExpandMore } from '@material-ui/icons'

interface IDetailProps {
  name: string
  height: number
  weight: number
  moves: any
  sprites: any
}

export const Detail = (props: IDetailProps) => {
  const {
    height,
    name,
    weight,
    sprites,
    moves,
  } = props
  const [expanded, setExpanded] = React.useState('')

  const handleChange = (panel: any) => (event: any, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : '')
  }

  return (
    <Card>
      <CardMedia
        image={sprites.front_shiny}
        title='Contemplative Reptile'
        style={{
          height: 100,
          width: 100,
          margin: '0 auto',
          backgroundSize: 'auto',
          marginTop: 10,
        }}
      />
      <CardContent>
        <Typography gutterBottom variant='h5' component='h2'>
          {name}
        </Typography>
        <Typography variant='body2' color='textSecondary' component='p'>
          Height: {height}
        </Typography>
        <Typography variant='body2' color='textSecondary' component='p'>
          Weight: {weight}
        </Typography>
        <br/>
        <ExpansionPanel expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMore />}
            aria-controls='panel4bh-content'
            id='panel4bh-header'
          >
            <Typography>Moves</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              {moves && moves.map((item: any, index: number) => item.move.name + ', ' )}
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </CardContent>
    </Card>
  )
}
