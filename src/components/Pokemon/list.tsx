import React from 'reactn'
import { IPokemon } from '@components/Pokemon/index'
import { Link } from 'react-router-dom'
import {
  Card,
  CardActions,
  CardActionArea,
  CardMedia,
  CardContent,
  Grid,
  Typography,
  Button,
  LinearProgress,
} from '@material-ui/core'

interface IListProps {
  item: IPokemon
  handleRemove?: (id: number) => void
  removable?: boolean
}

export const List = (props: IListProps) => {
  const { item, removable, handleRemove } = props
  return (
    <Card>
      <Link to={`/pokemon-detail/${item.name}`}>
        <CardActionArea>
          {item.fetching === undefined || item.fetching === true ?
            <LinearProgress />
          :
            <CardMedia
              image={item.avatar}
              title={item.nickname ? item.nickname : item.name}
              style={{
                height: 100,
                width: 100,
                margin: '0 auto',
                backgroundSize: 'auto',
                marginTop: 10,
              }}
            />
          }
        </CardActionArea>
      </Link>
      <CardContent>
        <Typography gutterBottom variant='h5' component='h2'>
          {item.nickname ? item.nickname : item.name}
        </Typography>
        {item.nickname &&
          <Typography gutterBottom variant='body2' component='p'>
            (type: {item.name})
          </Typography>
        }

        {item.fetching === undefined || item.fetching === true ?
          <LinearProgress />
        :
          <>
            <Typography variant='body2' color='textSecondary' component='p'>
              Height: {item.height}
            </Typography>
            <Typography variant='body2' color='textSecondary' component='p'>
              Weight: {item.weight}
            </Typography>
          </>
        }
      </CardContent>
      <CardActions>
        <Grid
          container
          direction='row'
          justify='space-between'
          alignItems='center'
        >
          <Grid item>
            <Link to={`/pokemon-detail/${item.name}`} style={{textDecoration: 'none'}}>
              <Button
                size='small'
                variant='outlined'
                color='default'
              >
              VIEW
              </Button>
            </Link>
          </Grid>
          {removable &&
            <Grid item>
              <Button
                id='release-button'
                size='small'
                variant='contained'
                color='secondary'
                onClick={() => handleRemove(item.id)}
              >
                RELEASE
              </Button>
            </Grid>
          }
        </Grid>
      </CardActions>
    </Card>
  )
}
