import { List } from '@components/Pokemon/list'
import { Detail } from '@components/Pokemon/detail'

interface ISprites {
  front_shiny: string
}

export interface IPokemon {
  id: number
  name: string
  url: string
  nickname: string
  avatar: string
  sprites: ISprites
  fetching: boolean
  height: number
  weight: number
  [x: string]: any
}

export {
  List,
  Detail,
}
