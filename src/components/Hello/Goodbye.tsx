import React, { setGlobal } from 'reactn'
import { Link } from 'react-router-dom'

const Goodbye = () => {
  setGlobal({
    authTokens: '',
    isError: false,
    isLoggedin: false,
    username: '',
  })

  return (
    <>
      <h3>You've been successfully logged out</h3>
      Click <Link to='/'>here</Link> to login.
    </>
  )
}

export default Goodbye
