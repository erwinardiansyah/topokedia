import React from 'react'
import {
  Button,
  TextField,
} from '@material-ui/core'

export interface IHelloProps {
  handleChange: (event: any) => void
  handleSubmit: (event: any) => void
  validated?: boolean
}

const Hello = (props: IHelloProps) => {
  const {
    handleChange,
    handleSubmit,
    validated,
  } = props
  return (
    <form noValidate onSubmit={ handleSubmit }>
      <TextField
        placeholder='Your username here...'
        variant='outlined'
        margin='normal'
        required
        fullWidth
        id='username'
        label='Username'
        name='username'
        autoFocus={ true }
        onChange={ handleChange }
        error={ !validated }
      />
      <Button
        type='submit'
        fullWidth
        variant='contained'
        color='primary'
        onClick={ handleSubmit }
      >
        Let's Go!
      </Button>
    </form>
  )
}

export default Hello
