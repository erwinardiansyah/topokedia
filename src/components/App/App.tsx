import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
} from 'react-router-dom'
import { setGlobal, useGlobal, getGlobal } from 'reactn'
import style from '@components/App/App.scss'
import { routes } from '@routes/index'
import { AuthContext } from '../../context/auth'
import { IPokemon } from '@components/Pokemon/index'
import {
  Container,
  Typography,
  Paper,
  Tab,
  Tabs,
} from '@material-ui/core'

const INITIAL_STATE = {
  authTokens: '',
  isError: false,
  isLoggedIn: false,
  username: '',
  user: {},
}

setGlobal(INITIAL_STATE)

export interface IUser {
  username: string
  catches: IPokemon[]
}

export interface IGlobal {
  authTokens: string
  user: IUser
  username: string
  isError: boolean
  isLoggedIn: boolean
  pokemon: IPokemon[]
}

const a11yProps = (index: number) => {
  return {
    'aria-controls': `nav-tabpanel-${index}`,
    'id': `nav-tab-${index}`,
  }
}

const LinkTab = (props: any) => {
  return (
    <Route render={({ history }) => (
      <Tab
        component='a'
        onClick={(event: any) => {
          event.preventDefault()
          history.push(event.currentTarget.getAttribute('to'))
        }}
        {...props}
      />
    )} />
  )
}

export const App = () => {
  const [authTokens, setAuthTokens] = useGlobal<IGlobal>('authTokens')
  const isLoggedIn = getGlobal<IGlobal>().isLoggedIn

  const [value, setTab] = React.useState(0)

  const setTokens = (token: string) => {
    localStorage.setItem('tokens', JSON.stringify(token))
    setAuthTokens(token)
  }

  const handleChangeTab = (event: any, newValue: any) => {
    setTab(newValue)
  }

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Container maxWidth='md'>
        <Router>
          {isLoggedIn &&
            <Paper style={{ marginBottom: 15 }}>
              <Tabs
                aria-label='nav tabs example'
                onChange={handleChangeTab}
                variant='fullWidth'
                value={value}
              >
                <LinkTab label='Pokemon List' to='/pokemon-list' {...a11yProps(0)} />
                <LinkTab label='My Pokemon' to='/my-pokemon-list' {...a11yProps(1)} />
              </Tabs>
            </Paper>
          }
          <div className={style.routeWrapper}>
            {!isLoggedIn && <Redirect to='/' /> }
            {routes.map((route: any, index: number) => (
              <Route key={index} {...route} />
            ))}
          </div>
        </Router>
        <Typography variant='body2' color='textSecondary' align='center' style={{
          bottom: 10,
          position: 'absolute',
          margin: 'auto',
          left: 0,
          right: 0,
          fontSize: 10,
        }}>
          {`Copyright © Tokopedia ${new Date().getFullYear()}.`}
        </Typography>
      </Container>
    </AuthContext.Provider>
  )
}
