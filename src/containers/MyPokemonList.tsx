import React from 'react'
import { getGlobal } from 'reactn'
import { IPokemon } from '@components/Pokemon/index'
import { IUser } from '@components/App/App'
import { IGlobal } from '@components/App/App'
import { List } from '@components/Pokemon/list'
import {
  CircularProgress,
  Grid,
  Typography,
} from '@material-ui/core'
import axios from 'axios'

interface IMyPokemonListState {
  errorMessage: string
  isError: boolean
  isFetching: boolean
  user: IUser
}

class MyPokemonList extends React.Component<{}, IMyPokemonListState> {
  public IS_MOUNTED = false

  constructor(props: any) {
    super(props)
    this.state = {
      errorMessage: '',
      isError: false,
      isFetching: true,
      user: {
        username: '',
        catches: [],
      },
    }
    this.handleRemove = this.handleRemove.bind(this)
  }

  public componentDidMount() {
    this.fetchPokemon()
    this.IS_MOUNTED = true
  }

  public componentWillUnmount() {
    this.IS_MOUNTED = false
  }

  public fetchPokemon = async () => {
    const user = getGlobal<IGlobal>().user
    if (user.catches) {
      user.catches.map((item, index) => {
        item.fetching = false
        item.avatar = item.sprites.front_shiny
        return item
      })
    } else {
      user.catches = []
    }
    this.setState({
      user,
      isFetching: false,
    })
  }

  public handleRemove = (e: any) => {
    const user = getGlobal<IGlobal>().user
    user.catches.map((item, index) => {
      if (item.id === e) {
        user.catches.splice(index, 1)
      }
      return item
    })
    this.setState({
      user,
      isFetching: false,
    })
  }

  public render() {
    const {
      errorMessage,
      isError,
      isFetching,
      user,
    } = this.state
    const { username, catches } = user

    return (
      <>
        {isFetching ?
          <CircularProgress />
        :
          <>
            <Typography gutterBottom={true} component='h1' variant='h5'>Hi, {username}!</Typography>
              { catches.length === 0 ?
                'No Data Found.'
              :
                <Grid container spacing={3}>
                  {catches.map( (item: IPokemon, index: number) => {
                    return(
                      <Grid key={index} item xs={6} sm={3}>
                        <List
                          key={index}
                          item={item}
                          removable={true}
                          handleRemove={() => this.handleRemove(item.id)}
                        />
                      </Grid>
                    )
                  })}
                </Grid>
              }
              { isError && <p> {errorMessage} </p>}
          </>
        }
        { isError && <p> {errorMessage} </p>}
      </>
    )
  }
}

export default MyPokemonList
