import React, { getGlobal, setGlobal } from 'reactn'
import { Redirect } from 'react-router-dom'
import { IGlobal } from '@components/App/App'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import {
  Container,
  Paper,
  Typography,
} from '@material-ui/core'

import Hello from '@components/Hello'

interface IHomeState {
  username?: string
  validated?: boolean
}

class Home extends React.Component<{}, IHomeState> {

  constructor() {
    super()
    this.state = {
      username: '',
      validated: true,
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  public handleChange = (event: any) => {
    this.setState({ username: event.target.value })
  }

  public handleSubmit = (event: any) => {
    const { username } = this.state
    const mock = new MockAdapter(axios)
    mock.onPost('/').reply(200, {
      token: 'random-token',
    })
    const user = {
      username,
    }
    if (username === '') {
      this.setState({validated: false})
    } else {
      axios.post('/', {
        username,
      }).then((result) => {
        if (result.status === 200) {
          setGlobal({
            authTokens: result.data.token,
            isError: false,
            isLoggedIn: true,
            username,
            user,
          })
        } else {
          setGlobal({
            authTokens: '',
            isError: true,
            isLoggedIn: false,
            username: '',
          })
        }
      }).catch((e) => {
        setGlobal({
          authTokens: '',
          isError: true,
          isLoggedIn: false,
          username: '',
        })
      })
    }
    event.preventDefault()
  }

  public render() {
    const isLoggedIn = getGlobal<IGlobal>().isLoggedIn
    const isError = getGlobal<IGlobal>().isError
    const { validated } = this.state
    return (
      <div>
      { isLoggedIn ?
        <>
          <Redirect to='/pokemon-list' />
        </>
        :
        <Container maxWidth='xs'>
          <Paper elevation={3} style={{ padding: 15 }}>
            <Typography component='h1' variant='h5'>Hello!</Typography>
            <Hello
              handleChange = {this.handleChange}
              handleSubmit = {this.handleSubmit}
              validated = {validated}
            />
            { isError && <span>Please try again later</span> }
          </Paper>
        </Container>
      }
    </div>
    )
  }
}

export default Home
