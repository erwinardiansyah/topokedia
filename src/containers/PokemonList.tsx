import React from 'react'
import { getGlobal } from 'reactn'
import { IPokemon } from '@components/Pokemon/index'
import { IGlobal } from '@components/App/App'
import { List } from '@components/Pokemon/list'
import {
  CircularProgress,
  Grid,
  Typography,
} from '@material-ui/core'
import axios from 'axios'

interface IPokemonListState {
  errorMessage: string
  isError: boolean
  isFetching: boolean
  pokemon: IPokemon[]
}

const axiosInstance = axios.create()

class PokemonList extends React.Component<{}, IPokemonListState> {
  public IS_MOUNTED = false

  constructor(props: any) {
    super(props)
    this.state = {
      errorMessage: '',
      isError: false,
      isFetching: true,
      pokemon: null,
    }
  }

  public componentDidMount() {
    this.fetchPokemonList()
    this.IS_MOUNTED = true
  }

  public componentWillUnmount() {
    this.IS_MOUNTED = false
  }

  public fetchPokemonList = async () => {
    await axiosInstance.get('https://pokeapi.co/api/v2/pokemon')
    .then((result) => {
      const isSuccess = result.status === 200
      this.setState({
        errorMessage: 'Please try again later',
        isError: isSuccess ? false : true,
        isFetching: false,
        pokemon: isSuccess ? result.data.results : null,
      }, () => {
        this.fetchPokemonDetails()
      })
    }).catch((e) => {
      this.setState({
        errorMessage: 'Please try again later',
        isError: true,
        isFetching: false,
      })
    })
  }

  public fetchPokemonDetails = () => {
    const { pokemon } = this.state
    pokemon.map( async (item: any, index: number) => {
      item.fetching = true
      await axiosInstance.get(`https://pokeapi.co/api/v2/pokemon/${item.name}`)
      .then((result) => {
        if (result.status === 200) {
          item = result.data
          item.fetching = false
          item.avatar = result.data.sprites.front_shiny
          pokemon[index] = item
          this.setState({ pokemon })
        }
      })
    })
  }

  public render() {
    const {
      errorMessage,
      isError,
      isFetching,
      pokemon,
    } = this.state
    const username = getGlobal<IGlobal>().username

    return (
      <>
        <Typography gutterBottom={true} component='h1' variant='h5'>Hi, {username}!</Typography>
        { isFetching ? <CircularProgress /> :
          <Grid container spacing={3}>
            {pokemon.map( (item: IPokemon, index: number) => {
              return(
                <Grid key={index} item xs={6} sm={3}>
                  <List item={item} />
                </Grid>
              )
            })}
          </Grid>
        }
        { isError && <p> {errorMessage} </p>}
      </>
    )
  }
}

export default PokemonList
