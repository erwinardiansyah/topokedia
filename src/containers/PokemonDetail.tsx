import React from 'react'
import { getGlobal, setGlobal } from 'reactn'
import { IGlobal } from '@components/App/App'
import { Detail } from '@components/Pokemon/detail'
import axios from 'axios'
import {
  CircularProgress,
  Typography,
  Button,
  Dialog,
  TextField,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core'

const axiosInstance = axios.create()

class PokemonDetail extends React.Component<any, any> {
  public IS_MOUNTED = false

  constructor(props: any) {
    super(props)
    const { name } = this.props.match.params
    const user = getGlobal<IGlobal>().user
    this.state = {
      isError: false,
      isFetching: true,
      errorMessage: '',
      pokemonName: name && name,
      pokemonData: null,
      user,
      isOpenDialog: false,
      isWinning: false,
      nickname: '',
    }

    this.handleCatch = this.handleCatch.bind(this)
  }

  public componentDidMount() {
    this.fetchSinglePokemon()
    this.IS_MOUNTED = true
  }

  public componentWillUnmount() {
    this.IS_MOUNTED = false
  }

  public fetchSinglePokemon = async () => {
    const { pokemonName } = this.state
    await axiosInstance.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
    .then((result) => {
      const isSuccess = result.status === 200
      this.setState({
        isError: !isSuccess,
        isFetching: false,
        errorMessage: isSuccess ? '' : 'Please try again later.',
        pokemonData: isSuccess && result.data,
      })
    }).catch((e) => {
      this.setState({
        isError: true,
        isFetching: false,
        errorMessage: 'Please try again later.',
      })
    })
  }

  public handleSetCatch = (nickname: string) => {
    const user = getGlobal<IGlobal>().user
    const { pokemonData } = this.state
    pokemonData.nickname = nickname
    user.catches = !user.catches ? [] : user.catches
    user.catches.push(pokemonData)

    this.setState({
      user,
      isOpenDialog: false,
    }, () => {
      setGlobal({
        user,
      })
    })
  }

  public handleCatch = () => {
    const arrayProbability = [1, 2]
    // tslint:disable-next-line:max-line-length
    const winningChance = arrayProbability[Math.floor(Math.random() * arrayProbability.length)]
    this.setState({
      isOpenDialog: true,
      isWinning: winningChance === 1,
    })
  }

  public renderModal = () => {
    const { isOpenDialog, isWinning, nickname } = this.state
    return (
      <Dialog
        open={isOpenDialog}
        onClose={() => this.setState({isOpenDialog: false})}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
      >
        <DialogTitle id='alert-dialog-title'>{isWinning ? 'Good catch!' : 'Failed!'}</DialogTitle>
          <DialogContent>
            <DialogContentText id='alert-dialog-description'>
              {isWinning ? 'Get your own nickname for this Pokemon!' : "You can catch 'em later."}
            </DialogContentText>
            { isWinning &&
              <TextField
                autoFocus
                margin='dense'
                id='nickname'
                label='Nickname'
                type='text'
                required
                fullWidth
                onChange={(event) => this.setState({nickname: event.target.value})}
                value={nickname}
              />
            }
            <DialogActions>
              <Button onClick={() => this.setState({isOpenDialog: false})} color='primary'>
                Close
              </Button>
              {isWinning &&
                <Button color='secondary' variant='contained' onClick={() => this.handleSetCatch(nickname)}>
                  Submit
                </Button>
              }
            </DialogActions>
          </DialogContent>
      </Dialog>
    )
  }

  public render() {
    const { pokemonData, isFetching, user } = this.state
    let isCatched = false
    if (pokemonData && user.catches) {
      isCatched = user.catches.includes(pokemonData)
      user.catches.map( (item: any) => {
        if (item.id === pokemonData.id) { isCatched = true }
      })
    }
    return (
      <div>
        { isFetching ?
          <CircularProgress />
        :
          pokemonData ?
            <>
              <Detail {...pokemonData} />
              <Button
                variant='contained'
                color='secondary'
                onClick={this.handleCatch}
                disabled={isCatched}
                size='large'
                style={{
                  width: '100%',
                  marginTop: 15,
                }}
              >{isCatched ? 'Catched' : "Catch 'em!"}</Button>
              {this.renderModal()}
            </>
          :
          <Typography variant='body2' color='textSecondary' component='p'>No data found.</Typography>
        }
      </div>
    )
  }
}

export default PokemonDetail
