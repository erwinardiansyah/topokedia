import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import PokemonList from '../../src/containers/PokemonList'
import { List } from '@components/Pokemon'

configure({ adapter: new Adapter() })

const mockState = {
  errorMessage: '',
  isError: false,
  isFetching: true,
  pokemon: [
    {
      id: 1,
      name: 'Rudi',
      url: 'Https://google.com',
      nickname: 'Ridu',
      avatar: 'png.jpeg',
      sprites: {
        front_shiny: 'png.jpeg',
      },
      fetching: false,
      height: 1,
      weight: 1,
    },
  ],
}

describe('Pokemon List Screen', () => {
  it('should render properly', () => {
    const componentRenderer = renderer.create(<PokemonList />)
    const tree = componentRenderer.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should contain List component', () => {
    mockState.isFetching = false
    const screen = shallow(<PokemonList />)
    screen.setState({ ...mockState })
    const instance = screen.instance()
    expect(instance.state).toEqual(mockState)
    instance.componentDidMount()
    expect(screen.find(List).exists()).toBeTruthy()
  })
})
