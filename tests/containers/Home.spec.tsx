import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Hello from '@components/Hello'
import Home from '../../src/containers/Home'

configure({ adapter: new Adapter() })

describe('Home Screen', () => {
  it('should render properly', () => {
    const componentRenderer = renderer.create(<Home />)
    const tree = componentRenderer.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should contain login form', () => {
    const wrapper = shallow(<Home />)
    expect(wrapper.find(Hello).exists()).toBeTruthy()
  })
})
