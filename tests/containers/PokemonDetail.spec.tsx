import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import PokemonDetail from '../../src/containers/PokemonDetail'
import { Detail } from '@components/Pokemon'

configure({ adapter: new Adapter() })

const mockState = {
  isError: false,
  isFetching: true,
  errorMessage: '',
  pokemonName: 'A',
  pokemonData: {},
  user: {
    username: 'A',
    catches: [
      {
        id: 1,
        name: 'Rudi',
        url: 'Https://google.com',
        nickname: 'Ridu',
        avatar: 'png.jpeg',
        sprites: {
          front_shiny: 'png.jpeg',
        },
        fetching: false,
        height: 1,
        weight: 1,
      },
    ],
  },
  isOpenDialog: false,
  isWinning: false,
  nickname: '',
}

const mockProps = {
  match: {
    params: {
      name: 'A',
    },
  },
}

describe('Pokemon Detail Screen', () => {
  it('should render properly', () => {
    const componentRenderer = renderer.create(<PokemonDetail {...mockProps} />)
    const tree = componentRenderer.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should contain Detail component', () => {
    mockState.isFetching = false
    const screen = shallow(<PokemonDetail {...mockProps} />)
    screen.setState({ ...mockState })
    const instance = screen.instance()
    expect(instance.state).toEqual(mockState)
    instance.componentDidMount()
    expect(screen.find(Detail).exists()).toBeTruthy()
  })
})
