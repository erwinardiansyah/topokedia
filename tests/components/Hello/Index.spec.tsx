import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Button } from '@material-ui/core'
import Hello from '@components/Hello'

configure({ adapter: new Adapter() })

const dummyComponentProps = {
  validated: true,
  handleChange: jest.fn(),
  handleSubmit: jest.fn(),
}

describe('Hello component', () => {
  it('should render component properly', () => {
    const componentRenderer = renderer.create(<Hello {...dummyComponentProps} />)
    const tree = componentRenderer.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should submit the form', () => {
    const component = shallow(<Hello {...dummyComponentProps} />)
    component.find('form').simulate('submit')
    expect(dummyComponentProps.handleSubmit)
      .toHaveBeenCalledTimes(1)
  })

  it('submit the form on button click', () => {
    const component = shallow(<Hello {...dummyComponentProps} />)
    component.find(Button).simulate('click')
    expect(dummyComponentProps.handleSubmit)
      .toHaveBeenCalledTimes(2)
  })
})
