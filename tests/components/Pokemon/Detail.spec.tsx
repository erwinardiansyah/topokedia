import React from 'react'
import renderer from 'react-test-renderer'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Detail } from '@components/Pokemon'

configure({ adapter: new Adapter() })

const dummyItem = {
  name: 'Rudi',
  sprites: {
    front_shiny: 'png.jpeg',
  },
  height: 1,
  weight: 1,
  moves: [
    {
      move: {
        name: 'A',
      },
    },
    {
      move: {
        name: 'B',
      },
    },
  ],
}

describe('Pokemon Detail component', () => {
  it('should render component properly', () => {
    const componentRenderer = renderer.create(<Detail {...dummyItem} />)
    const tree = componentRenderer.toJSON()
    expect(tree).toMatchSnapshot()
  })
})
