import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Button } from '@material-ui/core'
import { List } from '@components/Pokemon'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'

configure({ adapter: new Adapter() })

const history = createMemoryHistory()

const dummyItem = {
  id: 1,
  name: 'Rudi',
  url: 'Https://google.com',
  nickname: 'Ridu',
  avatar: 'png.jpeg',
  sprites: {
    front_shiny: 'png.jpeg',
  },
  fetching: false,
  height: 1,
  weight: 1,
}

const dummyComponentProps = {
  item: dummyItem,
  handleRemove: jest.fn(),
  removable: false,
}

describe('Pokemon List component', () => {
  it('should render component properly', () => {
    const componentRenderer = renderer.create(<Router history={history}><List {...dummyComponentProps} /></Router>)
    const tree = componentRenderer.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should release the Pokemon on button click', () => {
    dummyComponentProps.removable = true
    const component = shallow(<List {...dummyComponentProps} />)
    component.find(Button).at(1).simulate('click')
    expect(dummyComponentProps.handleRemove)
      .toHaveBeenCalledTimes(1)
  })
})
